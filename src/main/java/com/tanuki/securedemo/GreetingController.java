package com.tanuki.securedemo;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import no.finn.unleash.*;
import no.finn.unleash.util.*;
import no.finn.unleash.util.UnleashConfig;

@Controller
public class GreetingController {

	String appName;
	//appName = System.getvenc("APP_ENV")

	UnleashConfig config = UnleashConfig.builder()
				.appName("production")
				.instanceId("LU2yxdhbYnM-FjzynzFr")
				.unleashAPI("https://gitlab.com/api/v4/feature_flags/unleash/19951316")
				.build();

	Unleash unleash = new DefaultUnleash(config);

	@GetMapping("/greeting")

	public String greeting(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {

		if(unleash.isEnabled("upper_case_name")) {
		//do some magic
			model.addAttribute("name", name.toUpperCase());
			return "greeting";
		} else {
		//do old boring stuff
			model.addAttribute("name", name);
			return "greeting";
		}
	}
}
